﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class collectItem : MonoBehaviour
{
    AudioSource m_AudioSource;
    AudioClip m_AudioClip;
    public AudioClip m_AudioClip2;


    // Start is called before the first frame update
    void Start()
    {
                m_AudioSource = GetComponent<AudioSource>();
                m_AudioClip = m_AudioSource.clip;
    }

    // Update is called once per frame
    void OnTriggerEnter()
    {
        CollectObject();
    }

    void CollectObject()
    {
    m_AudioSource.Play();
    Destroy(gameObject, m_AudioClip.length);
    }

}
