﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToPointThenReset : MonoBehaviour
{
    public Vector3 EndPoint;
    public float speed;
    Vector3 StartPoint;
    float PassedTime;

    // Start is called before the first frame update
    void Start()
    {
        StartPoint = gameObject.transform.position;
        PassedTime = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        PassedTime += Time.deltaTime;
        transform.position = Vector3.Lerp(StartPoint, EndPoint, speed * PassedTime);
        if (transform.position == EndPoint)
        {
            PassedTime = 0.0f;
            transform.position = StartPoint;
        }
    }
}
