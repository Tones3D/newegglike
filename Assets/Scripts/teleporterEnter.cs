﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class teleporterEnter : MonoBehaviour
{
    public string chosenScene;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnTriggerEnter()
    {
        SceneManager.LoadScene(chosenScene, LoadSceneMode.Single);
    }

}
