﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mystCutscene : MonoBehaviour
{
    public string chosenScene;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Cutscene());
    }

    IEnumerator Cutscene()
    {
        print("Starting countdown");
        yield return new WaitForSeconds(21);
        GoBack();
    }

    void GoBack()
    {
        SceneManager.LoadScene(chosenScene, LoadSceneMode.Single);  
    }

}