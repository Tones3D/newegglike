﻿using UnityEngine;
using System.Collections;



public class danceCube : MonoBehaviour
{
    public Renderer rend;
    public MeshCollider collide;

    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = false;
        collide.enabled = false;

    }

    // Toggle the Object's visibility each second.
    void Update()
    {
        // Find out whether current second is odd or even
        bool oddeven = Mathf.FloorToInt(Time.time) % 2 == 0;


        // Enable renderer accordingly
        rend.enabled = oddeven;
        collide.enabled = oddeven;


    }
}