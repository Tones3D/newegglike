﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;



public class buttonhover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public bool isOver = false;

	public Animator anim;
	
	void Start()
    {
    }
	
    public void OnPointerEnter(PointerEventData eventData)
    {
		

        anim.Play("hover");
        isOver = true;
			
    }

    public void OnPointerExit(PointerEventData eventData)
    {

        anim.Play("unhover");
        isOver = false;
			
    }
}