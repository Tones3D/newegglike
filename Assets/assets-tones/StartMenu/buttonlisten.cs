﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class buttonlisten : MonoBehaviour
{
    //Make sure to attach these Buttons in the Inspector
    public Button b_load, b_new, b_set, b_quit;

	public Animator a_new;
	public Animator a_load;
	public Animator a_set;
	public Animator a_quit;
	
    void Start()
    {
        //Calls the TaskOnClick/TaskWithParameters/ButtonClicked method when you click the Button
        b_load.onClick.AddListener(b1);
        b_new.onClick.AddListener(b2);
		b_set.onClick.AddListener(b3);
        b_quit.onClick.AddListener(b4);
    }

    void b1()
    {
        a_load.Play("press");
    }

	void b2()
    {
        a_new.Play("press");
		StartCoroutine(b2_timed());
    }
	
    IEnumerator b2_timed()
    {
        yield return new WaitForSecondsRealtime(1);
		SceneManager.LoadScene("test-scene", LoadSceneMode.Single);
    }
	
	
    void b3()
    {
        a_set.Play("press");
    }
	
    void b4()
    {
        a_quit.Play("press");
		StartCoroutine(b4_timed());
    }
	
    IEnumerator b4_timed()
    {
        yield return new WaitForSecondsRealtime(1);
        Application.Quit();
		Debug.Log("Quit is ignored in-editor!");
    }
	
}