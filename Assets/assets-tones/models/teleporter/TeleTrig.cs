﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleTrig : MonoBehaviour
{
    [SerializeField] private Animator leftyAC;
	[SerializeField] private Animator rightyAC;
	
	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			leftyAC.SetBool("ifplayer_tele", true);
			rightyAC.SetBool("ifplayer_tele", true);
		}
	}
	
	private void OnTriggerExit(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			leftyAC.SetBool("ifplayer_tele", false);
			rightyAC.SetBool("ifplayer_tele", false);
		}
	}
	
}
